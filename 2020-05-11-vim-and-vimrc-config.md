# vim config

> zerostheory stream notes on setting up a vimrc file

----

## creating your file

Enter a terminal and be sure you're in your home directory and create your .vimrc file with `touch .vimrc`. This file starts with a dot making it hidden from normal list. You can view the file with `ls -a` for list all.

## start editing your new config file 

Begin with `vim .vimrc` to begin editing your .vimrc file.

Basic vim commands we'll need are:
* **i** for insert 
* **(esc)** to exit insert mode
* **:w** to write (save) 
* **:wq** to write and quit
* **hjkl** for navigation

Get into insert mode and enter something similar to this:

```
" My vimrc file

" Turn line numbers on by default, :set nonumber to disable
set number
syntax enable
```

Lines starting with quote marks are comments and not part of the actual script syntax.

Next we set the colorscheme.

```
" My vimrc file

" Turn line numbers on by default, :set nonumber to disable
set number
syntax enable

" set a colorscheme
set background dark
colorscheme elflord

" Make sure colorscheme keeps default background
hi Normal guibg=NONE ctermbg=NONE

```

Now download and install [Vim Plug](https://github.com/junegunn/vim-plug) and Pandoc which can probably be found in your package manager. 

Add the following vim plug scripts and a few basic vim plugins to your vimrc file.
```
"START the plugin manager vim-plug
call plug#begin()

if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif"

" Put plugins here starting with Plug 
Plug 'junegunn/goyo.vim'
Plug 'plasticboy/vim-markdown'
Plug 'vim-pandoc/vim-pandoc'
Plug 'https://gitlab.com/rwxrob/vim-pandoc-syntax-simple'

call plug#end()
```
You now have a color scheme and markdown pandoc syntax. 


