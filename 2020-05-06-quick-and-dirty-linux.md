# Ways to Linux

* Live boot

* Bare metal
	* The Only OS
	* Dual Boot

* Virtual
	* VirtualBox
	* VMware
	* KVM

## The OS Chosen - Ubuntu Mate

Why Ubuntu Mate? Ubuntu has one of the most beginner friendly installs as well as a large community to help with any issues. MATE desktop environment is very light weight, user friendly, and customizable.

## Other good choices for beginners

1. Linux Mint - Easy Install. Offers Cinnamon, XFCE, and MATE desktop environments. 
2. Pop_OS! - Easy Install. Ready for Gaming and Media out of the box. Offers Gnome.
3. Parrot OS Home - Easy install. Great Security and privacy out of the box, Offers MATE and KDE Desktops.
4. MX Linux - Designed for new users. Offers XFCE desktop.
5. Endeavour OS - Arch based. A choice of 8 Desktop Environments.
